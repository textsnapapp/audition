var mongoose = require ('mongoose');

var MessageSchema = new mongoose.Schema({
  body: String,
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  written: {type: Date, default: Date.now}
});

mongoose.model('Message', MessageSchema);
