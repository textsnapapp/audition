var supertest = require('supertest');
var superagent = require('superagent');
var assert = require('assert');
var app = require('../../app');
var user1 = superagent.agent(app);

describe('/GET all messages', function() {
  it('should get all the messages', function(done) {
    supertest(app)
      .get('/message/all')
      .expect(200)
      .end(function(err, res) {
        done();
      });
  });
});

var cookie;
var messageId;
describe('/POST a message', function() {
  before(function(done) {
    supertest(app)
      .post('/user/register')
      .send({'username': 'test1@hotmail.com', 'password': 'test2222'})
      .end(function() {
        supertest(app)
          .post('/user/login')
          .send({'username': 'test1@hotmail.com', 'password': 'test2222'})
          .end(function(err, res) {
            cookie = res.headers['set-cookie'];
            done();
          });
      });
  });

  it('should post a message', function(done) {
    //console.log('COOKIE: ' + JSON.stringify(cookie));
    supertest(app)
      .post('/message/')
      .set('cookie', cookie)
      .send({'body': 'race car'})
      .expect(200)
      .end(function(err, res) {
        //console.log('res: ' + JSON.stringify(res) );
        var result = JSON.parse(res.text);
        messageId = result._id;
        done();
      });
  });

});

describe('/GET check for palindrome', function() {

  it('should be a palindrome', function(done) {
    supertest(app)
      .get('/message/palindrome/' + messageId)
      .expect(200)
      .end(function(err, res) {
        //console.log('res: ' + JSON.stringify(res) );
        var result = JSON.parse(res.text);
        assert.ok(result.isPalindrome);
        done();
      });
  });
});

describe('/DELETE the message', function() {
  it('should delete the message', function(done) {
    supertest(app)
      .del('/message/' + messageId)
      .expect(200)
      .end(function(err, res) {
        done();
      });
  });

  it('should fail when trying to get the deleted message', function(done) {
    supertest(app)
      .get('/message/' + messageId)
      .expect(500)
      .end(done);
  });

});






