'use strict';

var _ = require('lodash');
var assert = require('assert');
var drome = require('../tools/palindrome.js');


describe('Palindome Tests', function() {

  describe('Should be a palindrome for messages like: ', function() {

    var messages = [
      'race car',
      'Star, go hang a salami. I\'m a lasagna hog. Rats.',
      'Sis',
      '',
      'a',
      'Hannah',
      '????*%^&%$******',
      'meeeeeeeeeeeeeeeeeeeeeeeeeem',
      '009900',
      '12345678987654321',
      '1234554321',
      '  ',
      '.&',
      'ANanNAna',
    ];

    _.each(messages, function(msg) {
      it(msg, function() {
        var isPalindrome = drome.isPalindrome(msg);
        assert.ok(isPalindrome);
      });
     });

  });


  describe('Should NOT be a palindrome for messages like: ', function() {

    var messages = [
      'hi',
      '8678N67t^&Hn8st7r%^E54evBR^&787NYM(8tntB',
      'hellllew',
      'mmmmmmmmmlzmmmmmmmmm',
      '028473',
      '00889800',
      'meeeeeeeeeeeeeeeeeeeeeeteeem'
    ];

    _.each(messages, function(msg) {
      it(msg, function() {
        var isPalindrome = drome.isPalindrome(msg);
        assert.notEqual(isPalindrome, true);
      });
     });

  });

});
