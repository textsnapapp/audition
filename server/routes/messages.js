var express = require('express');
var router = express.Router();
var _ = require('lodash');
var drome = require('../tools/palindrome.js');

var mongoose = require('mongoose');

var User = mongoose.model('User');
var Message = mongoose.model('Message');

/* GET messages */
router.get('/all', function(req, res, next) {
  var from = parseInt(req.query.from) || 0,
      size = parseInt(req.query.size) || 5,
      searchQuery = _.isEmpty(req.query.searchTerm) ? {} : { body: { $regex: req.query.searchTerm, $options: 'i' } };

  var query = Message.find(searchQuery);
  var sortMethod = {'written': -1};

  query
    .count() //Note this modifies the query so we use 'find' below
    .exec(function(err, count) {
      console.log('Message count: ' + count)
      //Execute the query
      query
        .sort(sortMethod)
        .skip(from)
        .limit(size)
        .exec('find', function(err, messages) { //Must include 'find' here otherwise we get undefined messages
          if(err){ return next(err);}
          console.log('Messages: ' + JSON.stringify(messages));
          res.json({
            count: count,
            append: (from !== 0),
            messages: messages
          });
        });
    });
});

/* POST message */
router.post('/', function(req, res, next) {
  var userid = _.get(req, 'user.id');
  var body = _.get(req, 'body.body');
  console.log('POST /message USER ID: ' + userid);
  if(!userid) {
    return res.status(500).json({err: 'User not logged in'});
  }

  var message = new Message({body: body});
  message.author = userid;
  console.log('POST /message: ' + message);
  message.save(function(err, message){
    if(err){ return next(err);}
    res.json(message);
  });
});

/* PARAM Preload a message object */
router.param('message', function(req, res, next, id) {
  var query = Message.findById(id);

  query.exec(function(err, message) {
    if(err) {return next(err);}
    if(!message) { return next(new Error("Can't find message")); }
    req.message = message;
    return next();
  });
});

/* GET Return single message*/
router.get('/:message', function(req, res) {
  console.log('Server: GET /message/:message');
  req.message.populate('author', function(err, message) {
    console.log('Message: ' + JSON.stringify(message));
    res.json(message);
  });
});

/* GET Check isPalindrome*/
router.get('/palindrome/:message', function(req, res) {
  var isPalindrome = drome.isPalindrome(req.message.body);
  console.log('isPalin: ' + isPalindrome);
  res.json({isPalindrome: isPalindrome});
});

/* DELETE message */
router.delete('/:message', function(req, res) {
  var id = _.get(req, 'params.message');
  var userid = _.get(req, 'user.id');
  console.log('Server: DELETE /:message with id: ' + id);

  //TODO Can only delete user's own message
  Message.remove({ _id: id }, function(err) {
    if(!err) {
      res.status(200).json({success: 'Successfully deleted message'});
    } else {
      return res.status(500).json({err: 'Failed to delete message: ' + err});
    }
  });
});


module.exports = router;
