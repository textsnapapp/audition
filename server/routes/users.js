var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    _ = require('lodash'),
    User = require('../models/User.js');

/*//GET users listing.
router.get('/', function(req, res) {
  res.send('respond with a resource');
});*/

router.get('/status', function(req, res) {
  console.log('Is Authenticated?: ' + req.isAuthenticated());
  if (!req.isAuthenticated()) {
    return res.status(200).json({
      status: false
    });
  }
  res.status(200).json({
    status: true,
    userid: _.get(req, 'user.id')
  });
});

router.post('/register', function(req, res) {
  console.log('Server: /register');
  User.register(new User({ username: req.body.username }), req.body.password, function(err, account) {
    if (err) {
      console.log('Error: ' + err);
      return res.status(500).json({err: err});
    }
    passport.authenticate('local')(req, res, function () {
      return res.status(200).json({status: 'Registration successful!'});
    });
  });
});

router.post('/login', function(req, res, next) {
  console.log('Server: /login');
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return res.status(500).json({err: err});
    }
    if (!user) {
      //Unauthorized
      return res.status(401).json({err: info});
    }
    req.logIn(user, function(err) {
      if (err) {
        return res.status(500).json({err: 'Could not log in user'});
      }
      res.status(200).json({status: 'Login successful!', userid: _.get(req, 'user.id')});
    });
  })(req, res, next);
});

router.get('/logout', function(req, res) {
  console.log('Server: /logout');
  req.logout();
  res.status(200).json({status: 'Bye!'});
});

module.exports = router;
