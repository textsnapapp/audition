var _ = require('lodash');

// Returns true if the message is a palindrome (the same backwards or forwards)
// Note, this function completely ignores non-alphanumeric characters
var isPalindrome = function(message) {
  var msg = message;
  msg = _.toLower(_.replace(msg, /[^a-zA-Z1-9]/gi, ''));

  var isPalindrome = true;
  var middle = msg.length / 2;
  for(var index = 0; index < middle; index++) {
    if(msg.charAt(index) !== msg.charAt(msg.length - index - 1)) {
      isPalindrome = false;
      break;
    }
  }

  return isPalindrome;
}
  


module.exports = {
  isPalindrome: isPalindrome
}
