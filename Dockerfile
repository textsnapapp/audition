FROM node:0.12.17

# Create app directory
RUN mkdir -p /var/www/app
WORKDIR /var/www/app

#** Not required since we will run a separate mongodb container
# Install MongoDB
#RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
#RUN echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list
#RUN apt-get update && apt-get install -y mongodb-org
#RUN mkdir -p /data/db

# Install Express
RUN npm install -g express-generator

# Install Git
RUN apt-get install -y git

# Install app dependencies
COPY package.json /var/www/app/
COPY bower.json /var/www/app/
RUN rm -rf node_modules && \
    npm install && \
    npm install -g bower && \
    bower install --allow-root

# Bundle app source
COPY . /var/www/app

EXPOSE 3002
CMD ["npm", "start"]
