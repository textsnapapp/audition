#!/usr/bin/env bash

# Stop containers
docker stop mongo
docker stop audition
docker rm -f mongo
docker rm -f audition

#Run
docker run -p 27017:27017 --name mongo -d mongo
docker run -it -p 3002:3002 --rm --link=mongo:mongo audition
