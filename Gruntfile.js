

module.exports = function(grunt) {

  grunt.initConfig({
    injector: {
      options: {},
      local_dependencies: {
        files: {
          'public/index.html': ['public/client/**/*.js', 'public/client/**/*.css'],
        }
      }
    }
  })


  //Load the plugin for the injector task
  grunt.loadNpmTasks('grunt-injector');

  //Default tasks
  grunt.registerTask('default', ['injector']);

};
