#!/usr/bin/env bash

# Stop containers
docker stop mongo
docker stop audition
docker rm -f mongo
docker rm -f audition

# Remove old images
docker rmi mongo
docker rmi audition

# Build
docker pull mongo
docker build -t audition .

