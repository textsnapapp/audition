var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var expressSession = require('express-session');
var hash = require('bcrypt-nodejs');
var passport = require('passport');
var localStrategy = require('passport-local' ).Strategy;
var debug = require('debug')('audition');

//Mongoose setup (must be done before requiring routes)
mongoose.connect('mongodb://172.17.0.1/audition');

//Models/Schema
var User = require('./server/models/User');
var Message = require('./server/models/Message');

//Create instance of Express
var app = express();

//Require Routes
var userRoutes = require('./server/routes/users.js');
var messageRoutes = require('./server/routes/messages.js');

//Define middleware
app.use('/public', express.static(__dirname + '/public'));
app.use('/apps', express.static(__dirname + '/public/apps'));
app.use('/client', express.static(__dirname + '/public/client'));
app.use('/images', express.static(__dirname + '/public/images'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

//app.use(favicon(__dirname + '/public/images/icons/icon-logo.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//Passport authentication
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

//Configure Passport
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Routes
app.use('/user', userRoutes);
app.use('/message', messageRoutes);

app.get('/*', function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

//Start Server listening on port 3002
app.set('port', process.env.PORT || 3002);

var server = app.listen(app.get('port'), function() {
  debug('Express server listening on port ' + server.address().port);
});


module.exports = app;
