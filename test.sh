#!/usr/bin/env bash

# Stop the main app
docker stop audition

# Make sure mongodb container is running
docker run -p 27017:27017 --name mongo -d mongo

if ! [ -x "$(command -v npm)" ] || ! [ -x "$(command -v node)" ]; then
  curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

if [ ! -d "node_modules" ]; then
  npm install
fi

npm test
