angular.module('audition', ['ui.router', 'ngRoute', 'ngMaterial', 'ngAnimate', 'ngMessages'])

  .config(function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
  })

  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/home');
  
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  
    $stateProvider.state('home', {
      url: '/home',
      templateUrl: '../client/views/messages/list.html',
      controller: 'ListCtrl',
      resolve: {
        //Load the user state (logged in/out) before loading the page
        userPromise: function(AuthService) {
          return AuthService.getUserStatus();
        },
        messagePromise: function(messageFactory) {
          console.log('/home route resolve');
          return messageFactory.getAll(5, 0)
            .then(function(messages) {
              console.log('/home route messages: ' + JSON.stringify(messages));
              return messages;
            })
            .catch(function(err) {
              console.log('/home route messagePromise failed: ' + err);
            });
        }
      }
    })
    .state('message', {
      url: '/msg/{id}',
      templateUrl: '../client/views/messages/message.html',
      controller: 'MessageCtrl',
      resolve: {
        message: function($stateParams, messageFactory) {
          console.log('/msg/{id} route resolve id: ' + $stateParams.id);
          return messageFactory.getMessage($stateParams.id);
        },
        //Load the user state (logged in/out) before loading the page
        userPromise2: function(AuthService) {
          return AuthService.getUserStatus();
        }
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: '../client/views/auth/login.html',
      controller: 'loginController'
    })
    .state('register', {
      url: '/register',
      templateUrl: '../client/views/auth/register.html',
      controller: 'registerController'
    });
  
  });
