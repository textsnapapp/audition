(function() {
"use strict";

  angular.module('audition')
    .directive('messageSubmit', function(AuthService, messageFactory, $mdToast){
      return {
        restrict: 'E',
        templateUrl: 'client/components/message-submit/message-submit.html',
        link: link,
        scope: true
      };

      function link(scope, elem, attr) {

        scope.createMessage = function() {
          var msg = scope.input.message;
          if(msg) {
            if(!AuthService.isLoggedIn()) {
              $mdToast.show(
                $mdToast.simple()
                  .textContent('Login to post a Message!')
                  .position('top right')
                  .hideDelay(3000)
              );
            }
            else {
              console.log('Creating new message: ' + msg);
              messageFactory.create(msg);
              scope.input.message = "";
              scope.messageInputForm.$setPristine();
            }
          }
        };

      }

    });

})();
