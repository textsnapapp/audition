angular.module('audition')
  .factory('messageFactory', function($http, AuthService) {

    var o = {
      messages: [],
      total: 0
    };

    //GET All messages
    o.getAll = function(size, from, searchTerm, mine) {
      console.log('messageFactory getAll()');
      return $http({
        url: '/message/all',
        method: 'GET',
        params: {size: size, from: from, searchTerm: searchTerm, mine: mine }
      })
      .success(function(data) {
        console.log('messageFactory getAll() success ' + JSON.stringify(data));
        if(data.append && _.isArray(o.messages) && _.isArray(data.messages)) {
          o.messages = _.concat(o.messages, data.messages);
        }
        else {
          angular.copy(data.messages, o.messages);
        }
        o.total = data.count;
        return o.messages;
      })
      .error(function(err) {
        console.log('messageFactory getAll() failed: ' + err);
        return {};
      });
    };

    //POST Create message
    o.create = function(msg) {
      console.log('messageFactory create: ' + msg);
      return $http.post('/message', {body: msg})
        .success(function(data) {
          o.messages.unshift(data);
          o.total++;
          console.log('messageFactory create: ' + JSON.stringify(data));
        });
    };

    //GET message
    o.getMessage = function(id) {
      return $http({
        url: '/message/' + id,
        method: 'GET',
      })
      .success(function(data) {
        console.log('messageFactory getMessage: ' + JSON.stringify(data));
        return data;
      });
    };

    //DELETE message
    o.delete = function(id) {
      return $http.delete('/message/' + id)
        .success(function(data) {
          console.log('messageFactory delete id: ' + id);
          _.remove(o.messages, {_id: id});
          o.total--;
        });
    };

    //GET isPalindrome?
    o.isPalindrome = function(id) {
      return $http({
        url: '/message/palindrome/' + id,
        method: 'GET',
      })
      .success(function(data) {
        return data;
      });
    };

    return o;

  });
