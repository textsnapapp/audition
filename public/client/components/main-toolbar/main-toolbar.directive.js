(function() {
"use strict";

  angular.module('audition')
    .directive('mainToolbar', function(AuthService, $state){
      return {
        restrict: 'E',
        templateUrl: 'client/components/main-toolbar/main-toolbar.html',
        link: link,
        scope: true
      };

      function link(scope, elem, attr) {
        scope.$watch(function() {
          return AuthService.isLoggedIn();
        }, function(newvalue, oldValue) {
          scope.isLoggedIn = AuthService.isLoggedIn();
        });
      }

    });

})();
