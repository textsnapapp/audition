angular.module('audition')
  .factory('AuthService', function($q, $timeout, $http) {

    // create user variable
    var user = null;
    var userid = null;

    // return available functions for use in controllers
    return ({
      isLoggedIn: isLoggedIn,
      getUserId: getUserId,
      getUserStatus: getUserStatus,
      login: login,
      logout: logout,
      register: register
    });

    function isLoggedIn() {
      return !!user;
    }

    function getUserId() {
      return userid;
    }

    function getUserStatus() {
      return $http.get('/user/status')
        .success(function (data) {
          if(data.status){
            user = true;
            userid = _.get(data, 'userid');
          } else {
            user = false;
            userid = null;
          }
        })
        .error(function (data) {
          user = false;
          userid = null;
        });
    }

    function login(username, password) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/user/login', {username: username, password: password})
        // handle success
        .success(function (data, status) {
          console.log('Login data: ' + JSON.stringify(data));
          if(status === 200 && data.status){
            user = true;
            userid = _.get(data, 'userid');
            deferred.resolve();
          } else {
            user = false;
            userid = null;
            deferred.reject();
          }
        })
        // handle error
        .error(function (data) {
          user = false;
          userid = null;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    function logout() {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a get request to the server
      $http.get('/user/logout')
        // handle success
        .success(function (data) {
          user = false;
          userid = false;
          deferred.resolve();
        })
        // handle error
        .error(function (data) {
          user = false;
          userid = false;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    function register(username, password) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/user/register', {username: username, password: password})
        // handle success
        .success(function (data, status) {
          if(status === 200 && data.status){
            deferred.resolve();
          } else {
            deferred.reject();
          }
        })
        // handle error
        .error(function (data) {
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

});
