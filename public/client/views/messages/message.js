angular.module('audition')
  .controller('MessageCtrl', function($scope, messageFactory, message, AuthService) {
    $scope._ = _;
    $scope.message = message;
    $scope.userid = AuthService.getUserId();
    $scope.drome = {};

    messageFactory.isPalindrome(message.data._id)
      .then(function(data) {
        console.log('Palindrome data: ' + JSON.stringify(data));
        $scope.drome.isPalindrome = !!data.data.isPalindrome;
      });

  });
