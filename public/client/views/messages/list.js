angular.module('audition')
  .controller('ListCtrl', function($scope, messageFactory, AuthService, $mdToast, $mdDialog) {
    $scope.messages = messageFactory.messages;
    $scope.userid = AuthService.getUserId();

    $scope.paging = {};
    $scope.paging.size = 5;
    $scope.paging.loading = false;
    $scope.paging.total = messageFactory.total;

    $scope.delete = function(ev, message) {
      var confirm = $mdDialog.confirm()
            .title('Would you like to delete this message?')
            .textContent(message.body)
            .ariaLabel('Delete message')
            .targetEvent(ev)
            .ok('Do it!')
            .cancel('No!!!');

      $mdDialog.show(confirm).then(function() {
        messageFactory.delete(message._id);
        $mdToast.show(
          $mdToast.simple()
            .textContent('Message Deleted')
            .position('top right')
            .hideDelay(3000)
        );
      }, function() {
      });
    };

    $scope.loadMore = function() {
      if(messageFactory.total > $scope.messages.length) {
        $scope.paging.loading = true;
        return messageFactory.getAll($scope.paging.size, $scope.messages.length)
          .then(function() {
            console.log('Got messages' + $scope.messages);
          })
          .finally(function() {
            $scope.paging.loading = false;
          });
      }
    };

    $scope.$watchCollection(function() {
      return messageFactory;
    }, function(newVal, oldVal) {
      if(newVal) {
        $scope.messages = messageFactory.messages;
        $scope.paging.total = messageFactory.total;
      }
    });

  });
