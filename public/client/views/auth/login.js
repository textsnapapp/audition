angular.module('audition')
  .controller('loginController', function ($scope, $location, AuthService) {

    console.log('User status: ' + AuthService.getUserStatus());

    $scope.login = function () {
      $scope.error = false;
      $scope.disabled = true;

      // call login from service
      AuthService.login($scope.loginForm.username, $scope.loginForm.password)
        .then(function () {
          console.log('Login service called successfully');
          $location.path('/');
          $scope.disabled = false;
          $scope.loginForm = {};
        })
        .catch(function () {
          console.log('Login service call failed');
          $scope.error = true;
          $scope.errorMessage = "Invalid username and/or password";
          $scope.disabled = false;
          $scope.loginForm = {};
        });
    };

  });
