angular.module('audition')
  .controller('registerController', function ($scope, $location, AuthService) {

    console.log(AuthService.getUserStatus());

    $scope.register = function () {
      // Initial values
      $scope.error = false;
      $scope.disabled = true;

      // Call register from service
      AuthService.register($scope.registerForm.username, $scope.registerForm.password)
        // Handle success
        .then(function () {
          $location.path('/home');
          $scope.disabled = false;
          $scope.registerForm = {};
        })
        // Handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.disabled = false;
          $scope.registerForm = {};
        });
    };

  });
