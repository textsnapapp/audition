angular.module('audition')
  .controller('logoutController', function ($scope, $location, AuthService) {

    console.log('Logging out');

    $scope.logout = function () {
      console.log('User status: ' + AuthService.getUserStatus());
      // call logout from service
      AuthService.logout()
        .then(function () {
          $location.path('/login');
        });
    };

  });
