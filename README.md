# Palindrome Audition

Palindrome Audition is an application build using the MEAN stack. An Express server runs in the background and listens for requests. The backend REST api, written in Node, services incoming HTTP requests. Supported operations include adding, removing, and listing messages. There is also a route to determine if a message is a palindrome. Messages are stored in a MongoDB database. Palindrome Audition can also be accessed through the web. An Angular front-end provides a user interface for doing operations with messages as previously listed. The Angular architecture itself makes use of modular Directives and Services to provide loosely coupled UI elements and to make calls to the backend server.

## Repository Location
https://bitbucket.org/textsnapapp/audition/

## Build and Deploy
### On an AWS EC2 Ubuntu 16.06 server with Docker run the following:
`git clone git@bitbucket.org:textsnapapp/audition.git`  
`cd audition`  
`./start.sh`  

## Tests
`./test.sh`  
Note that Testing will test against an existing mongodb docker container

## Demo
http://ec2-35-163-32-220.us-west-2.compute.amazonaws.com:3002

## Routes
  
**Title: ** Show all messages  
**URL: ** /message/all  
**Method: ** GET  
**URL Params: ** Optional: from=[integer] size=[integer] searchTerm=[string]  
**Response Codes: ** Success (200 OK) Internal Server Error (500)

**Title: ** Create new message  
**URL: ** /message  
**Method: ** POST  
**Data Params: ** Required: {body: [string]}  
**Response Codes: ** Success (200 OK) Internal Server Error (500)  

**Title: ** Get single message  
**URL: ** /message/:messageId  
**Method: ** GET  
**Response Codes: ** Success (200 OK) Internal Server Error (500)  

**Title: ** Check if message is a palindrome  
**URL: ** /message/palindrome/:messageId  
**Method: ** GET  
**Response Codes: ** Success (200 OK) Internal Server Error (500)  

**Title: ** Delete a message  
**URL: ** /message/:messageId  
**Method: ** DELETE  
**Response Codes: ** Success (200 OK) Internal Server Error (500)  

**Title: ** Get user login status  
**URL: ** /user/status  
**Method: ** GET  
**Response Codes: ** Success (200 OK)  

**Title: ** Register a user  
**URL: ** /user/register  
**Method: ** POST  
**Data Params: ** Required: username=[string] password=[string]  
**Response Codes: ** Success (200 OK) Internal Server Error (500)  

**Title: ** User login  
**URL: ** /user/login  
**Method: ** POST  
**Data Params: ** Required: username=[string] password=[string]  
**Response Codes: ** Success (200 OK) Unauthorized (401) Internal Server Error (500)  

**Title: ** User logout  
**URL: ** /user/logout  
**Method: ** GET  
**Response Codes: ** Success (200 OK)  

## Sequence Diagrams of Use Case Interactions

![List of Messages](https://bytebucket.org/textsnapapp/audition/raw/e595538d05736fc83ba37afa91de9c8f262344db/public/images/showlistofmessages.png)

![Specific message](https://bytebucket.org/textsnapapp/audition/raw/4724c6b2383d73c0a255fbe90c6a62485495bc19/public/images/specificmessage.png)

![Submit Message](https://bytebucket.org/textsnapapp/audition/raw/4724c6b2383d73c0a255fbe90c6a62485495bc19/public/images/submitmessage.png)

![Delete Message](https://bytebucket.org/textsnapapp/audition/raw/4724c6b2383d73c0a255fbe90c6a62485495bc19/public/images/deletemessage.png)

## Troubleshooting
### Upload keygen to BitBucket to clone repo
 `ssh-keygen -t rsa'
 Copy ~/.ssh/id_rsa.pub to BitBucket
### If you need to install docker, follow these instructions
 # https://docs.docker.com/engine/installation/linux/ubuntulinux/  
 # Remember to add user to the group  
 # Remember to logout and log back in  
### To remove docker images taking up space
 `docker rm $(docker ps -aq)`
### To kill docker container
 `docker rm -f <name>`
### Running Tests Mongoose error
 https://stackoverflow.com/questions/37654497/cannot-find-module-mongodb-node-modules-bson


## License

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
